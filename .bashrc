#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Aliases
alias ls='ls --color=auto'

alias config='/usr/bin/git --git-dir=/home/pudding/.dotfiles/ --work-tree=/home/pudding'

PS1="[\A] \u @ \h >\n\w \\$ \[$(tput sgr0)\]"

#startx

